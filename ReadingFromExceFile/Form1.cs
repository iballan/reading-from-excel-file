﻿using BUM_FINAL_ID3.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BUM_FINAL_ID3
{
    public partial class Form1 : Form
    {
        TreeDataSource excelFileDataSource;
        List<TreeAttribute> attributes;
        TreeAttribute targetAttribute;
        Tree tree;

        public Form1()
        {
            InitializeComponent();
        }

        private void open_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel files (*.xlsx)|*.xlsx";
            dialog.Title = "Please select an excel file.";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // 
                excelFileDataSource = new TreeDataSource(dialog.FileName);
                dataGridView1.DataSource = excelFileDataSource;
                setRowNumber(dataGridView1);
                attributes = excelFileDataSource.getAttribiutes();
                comboBox1.Items.Clear();
                foreach (TreeAttribute att in attributes)
                    comboBox1.Items.Add(att);
                comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
                targetAttribute = attributes.ElementAt(attributes.Count - 1);
            }
        }
        /* 
         * To get possible values of each attribute,
         *      only used for testing till now.
         */ 
        private void possibleValues_button_Click(object sender, EventArgs e)
        {
            resultTextBox.Clear();

            int counter = 0;
            List<TreeAttribute> attributes = excelFileDataSource.getAttribiutes();
            int numOfAttributes = attributes.Count;
            foreach (TreeAttribute attribute in attributes)
            {

                if(counter == numOfAttributes - 1) // check whether it is last attribute or not.
                    resultTextBox.AppendText("Result: " + attribute.Name);
                else
                    resultTextBox.AppendText("Attribute: " + attribute.Name);

                string values = "";
                List<String> possibleValues = attribute.PossibleValues;

                foreach (string value in possibleValues)
                {
                    values += value.ToUpper() + ", ";
                }

                resultTextBox.AppendText("\r\n\t" + values + " ("+ possibleValues.Count +")" +  "\r\n");
                counter++;
            }
            
            //DecisionTreeID3.runID3(excelFileDataSource, targetAttribute);
        }


        private void setRowNumber(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
            dgv.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
        }

        private void Form1_Load(object sender, EventArgs e)
        { 
            //double entro = AlgoCalculations.calculateEntropy(new int[]{4,3,3});
            //resultTextBox.AppendText(entro+"\r\n");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = comboBox1.SelectedIndex;
            Object selectedItem = comboBox1.SelectedItem;
            targetAttribute = (TreeAttribute)selectedItem;
            //MessageBox.Show("Selected Item Text: " + selectedItem.ToString() + "\n" +
            //                "Index: " + selectedIndex.ToString());
        }

        private void createTreeButton_Click(object sender, EventArgs e)
        {
            
            // the code that you want to measure comes here
            
            
            treeTextBox.Clear();

            // Measuring the performance 
            var watch = Stopwatch.StartNew(); // start timer as a real time
            TimeSpan begin = Process.GetCurrentProcess().TotalProcessorTime; // start time as a Process Time

            // starting the algorithm
            DecisionTreeID3 decisionTreeId3 = new DecisionTreeID3(excelFileDataSource, targetAttribute);
            tree = decisionTreeId3.runID3();

            // stop measuring performance
            watch.Stop();
            TimeSpan end = Process.GetCurrentProcess().TotalProcessorTime;

            resultTextBox.AppendText("\r\nTotal Number of Records = " + excelFileDataSource.Rows.Count +"\r\n");
            resultTextBox.AppendText("(Processing Time)Time by TimeSpan: " + (end - begin).TotalMilliseconds + " ms.\r\n");
            resultTextBox.AppendText("(Real Time)Time by StopWatch: "+ TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds).TotalSeconds+" ms.\r\n");

            // print out the result
            treeView1.Nodes.Clear(); // first clear old results

            treeTextBox.Text = tree.BuildString(); // print new tree
            tree.drawTree(treeView1); // draw the tree to treeView
        }

        private void showOptimalPath_Click(object sender, EventArgs e)
        {
            //DataTable dataTable = excelFileDataSource.Clone();
            ShowOptimalPathForm showOptimalForm = new ShowOptimalPathForm(attributes, tree, targetAttribute, excelFileDataSource.Clone());
            //showOptimalForm.Tree = tree;
            //showOptimalForm.DataTable = excelFileDataSource.Clone(); // Its important to be clone
            //showOptimalForm.TargetAttribute = targetAttribute;
            //showOptimalForm.Attributes = attributes;
            showOptimalForm.Show();
        }
    }
}
