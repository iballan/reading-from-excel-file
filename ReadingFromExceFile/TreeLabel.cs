﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUM_FINAL_ID3
{
    class TreeLabel
    {
        private string _strValue;
        private double _value;

        public TreeLabel(string strValue, double value)
        {
            _strValue = strValue;
            _value = value;
        }

        public TreeLabel()// start as not a leaf node and modify after...
        {
            _strValue = "Not a leaf node";
            _value = -1;
        }

        public string StrValue
        {
            get
            {
                return _strValue;
            }
        }

        public double Value
        {
            get
            {
                return _value;
            }
        }
    }
}
