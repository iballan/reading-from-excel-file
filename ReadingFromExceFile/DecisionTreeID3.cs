﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using BUM_FINAL_ID3.Structure;

namespace BUM_FINAL_ID3
{
    class DecisionTreeID3
    {
        // The classes attribute (Target Attribute)
        TreeAttribute _targetAttribute;
        TreeDataSource _samples;

        public DecisionTreeID3(TreeDataSource samples, TreeAttribute targetAttribute)
        {
            _samples = samples;
            _targetAttribute = targetAttribute;
        }

        // this will be main function that we should execute in order to start the algorithm
        public Tree runID3()
        {
            TreeNode node = startAlgorithm(_samples, _targetAttribute, null);
            Tree tree = new Tree("Root");
            tree.Add(node); 
            Console.WriteLine(tree.BuildString());
            return tree;
        }

        /// <summary>
        /// Will start for the first time, then it will be called recursively from another function<string, int>
        /// </summary>
        /// <param name="dataSamples">TreeDataSource table that will contain the data to be processed<string, int>.</param>
        /// <returns>TreeNode that will be attached to the upper level.</returns>
        public TreeNode startAlgorithm(TreeDataSource dataSamples, TreeAttribute targetClassAttribute, 
            TreeAttribute attributeToContinueFrom)
        {
            TreeNode treeNode;
            // step 1 - If all instances in C are positive, then create YES node and halt.
            //      If all instances in C are negative, create a NO node and halt.
            if (attributeToContinueFrom != null)
                dataSamples.Columns.Remove(attributeToContinueFrom.Name);


            if (dataSamples.Columns.Count == 1)
            {
                // here is a trick
                return new TreeNode(getRandomTargetAttributeElement(targetClassAttribute));
            }

            Tuple<string, int>[] targetAttributeOccurrences =
                AlgoCalculations.getOccurrenceOfAttrinbuteAsTupleArray(dataSamples, targetClassAttribute);

            Tuple<string, int> isAllSameResult = AlgoCalculations.isAllValuesAreSame(targetAttributeOccurrences);

            if (isAllSameResult != null)
            {
                // here it halts, and return one node that contains the class which all r same
                treeNode = new Tree(isAllSameResult.Item1);
            }
            else 
            {
                // step 2 - get the best entropy, and it will be the root of the tree
                List<TreeAttribute> listOfAttributes = dataSamples.getAttribiutes();
                TreeAttribute bestAttribute = findBestAttribute(listOfAttributes, dataSamples, targetClassAttribute);
                if (bestAttribute.Name == targetClassAttribute.Name)
                    return new Tree(targetClassAttribute.PossibleValues.ElementAt(0));
                treeNode = new TreeNode(bestAttribute.Name);
                Dictionary<string, TreeNode> possibleValueTreeNode = new Dictionary<string, TreeNode>();
                foreach (string possibleValue in bestAttribute.PossibleValues)
                {
                    TreeNode branch = new TreeNode(possibleValue);
                    treeNode.Add(branch);
                    possibleValueTreeNode.Add(possibleValue, branch);
                    TreeNode node = startAgain(listOfAttributes, dataSamples,_targetAttribute, bestAttribute, possibleValue);
                    branch.Add(node);
                }

            }
            return treeNode;
        }

        /// <summary>
        /// Will start to check for each attribute's value, it will be the next step of the algorithm<string, int>
        /// </summary>
        /// <param name="attributes"> Attributes to be processed on the data (dynamically called cuz it will be decresed) <List<TreeAttribute>>.</param>
        /// <param name="dataSample"> TreeDataSource table that will contain the data to be processed <TreeDataSource>.</param>
        /// <param name="targetClassAttribute">Target (class) attribute <TreeAttribute>.</param>
        /// <param name="attributeOfPossibleValue"> Attribute that is parent of the Possible value <TreeAttribute>.</param>
        /// <param name="possibleValue"> Possible value is a string of value of attribute, that will be a branch of the tree <string>.</param>
        /// <returns>TreeNode that will be attached to the upper level.</returns>
        public TreeNode startAgain(List<TreeAttribute> attributes, TreeDataSource dataSample ,TreeAttribute targetClassAttribute, TreeAttribute attributeOfPossibleValue, string possibleValue)
        {
            DataTable result = dataSample.Rows
                              .Cast<DataRow>()
                              .Where(x => x[attributeOfPossibleValue.Name].ToString() == possibleValue).CopyToDataTable();

            TreeDataSource newSamples = new TreeDataSource(result);

            result.Clear();

            var isAllYes = newSamples.AsEnumerable()
                .Select(s => s.Field<string>(targetClassAttribute.Name))
                .Distinct()
                .ToList();

            var first = isAllYes.First();

            if (isAllYes.All(x => x == first))
            {
                // YES (ALL SAME)
                return new TreeNode(first.ToString());
            }
            else
            {
                //return new TreeNode("?");  // <<<<<<<<<<< TEST
                return startAlgorithm(newSamples, targetClassAttribute, attributeOfPossibleValue);
            }
        }

        /// <summary>
        /// Will find the best attribute to be considered as the next node of the tree<string, int>
        /// </summary>
        /// <param name="attributes"> Attributes to be processed on the data (dynamically called cuz it will be decresed) <List<TreeAttribute>>.</param>
        /// <param name="dataSample"> TreeDataSource table that will contain the data to be processed <TreeDataSource>.</param>
        /// <param name="targetClassAttribute">Target (class) attribute <TreeAttribute>.</param>
        /// <returns>best attribute to be considered as the next node of the tree.</returns>
        public TreeAttribute findBestAttribute(List<TreeAttribute> attributes, TreeDataSource dataSample, TreeAttribute targetClassAttribute)
        {
            // now we should calculate the entropy for the target attribute
            Tuple<string, int>[] listOfOccuerencesAsTuples = AlgoCalculations.getOccurrenceOfAttrinbuteAsTupleArray(dataSample, targetClassAttribute);

            double entropyOfTargetAttribute = AlgoCalculations.calculateEntropyForTuplesArray(listOfOccuerencesAsTuples);

            //attributes = _samples.getAttribiutes();
            Dictionary<TreeAttribute, double> attributeGainDic = new Dictionary<TreeAttribute, double>();

            attributes.Remove(targetClassAttribute);
            foreach (TreeAttribute attribute in attributes)
            {
                if (attribute.Name != targetClassAttribute.Name)
                {
                    //double entropyForThisAttribute = AlgoCalculations.calculateEntropy(_samples, attribute);
                    double gainInfo = AlgoCalculations.calculateGainInformation(dataSample, attribute,
                        entropyOfTargetAttribute, targetClassAttribute);
                    attributeGainDic.Add(attribute, gainInfo);
                }
            }

            //int index;
            //TreeAttribute bestAttribute;
            //double max = Double.MinValue
            //foreach (var keyValue in attributeGainDic)
            //{
            //    if (keyValue.Value > max)
            //    {
            //        bestAttribute = keyValue.Key;
            //    }
            //}

            // alternative of the above
            var pairOfMax = attributeGainDic.FirstOrDefault(x => x.Value == attributeGainDic.Values.Max());


            return pairOfMax.Key;
        }

        public string getRandomTargetAttributeElement(TreeAttribute targetAttribute)
        {
            Random r = new Random();
            int rInt = r.Next(0, targetAttribute.PossibleValues.Count); //for integers
            return targetAttribute.PossibleValues.ElementAt(rInt);
        }

    }
}
