﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using Excel;
using System.Threading.Tasks;

namespace BUM_FINAL_ID3
{
    class TreeDataSource : DataTable
    {
        string _tableName = "Data_Table";
        string _filePath;

        public TreeDataSource(string filePath)
        {
            this.TableName = _tableName;
            _filePath = filePath;
            Load();
        }

        public TreeDataSource()
        {

        }

        public TreeDataSource(DataTable table)
        {

            load(table);

            //Console.WriteLine("Successfully Imported");
        }

        private void load(DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                this.Columns.Add(column.ColumnName, column.DataType);
            }

            foreach (DataRow row in table.Rows)
            {
                this.ImportRow(row);
            }

            //Console.WriteLine("Successfully Imported");
        }

        public void Load()
        {
            FileStream stream = File.Open(_filePath, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = true;
            DataSet resultDataSet = excelReader.AsDataSet();

            load(resultDataSet.Tables[0]);

            //Console.WriteLine("Successfully Imported");

            printTable();
        }

        public void printTable() {
            Console.WriteLine("# Rows = "+this.Rows.Count+", # Columns = "+this.Columns.Count);
            string columns = "";
            foreach ( DataColumn column in this.Columns) {
                columns += column.ColumnName + ", ";
            }
            Console.WriteLine("Columns: " + columns);
        }

        #region TODO: LOOK AT THIS CASTING DOWN HERE
        public List<string> getPossibleValuesOfColumn(string columnName)
        {
            List<string> tempPossibleValueList = new List<string>();
            foreach (DataRow row in this.Rows)
            {
                foreach (DataColumn column in this.Columns)
                {
                    if (column.ColumnName.ToUpper() == columnName.ToUpper())
                    {
                        if(!tempPossibleValueList.Contains(row[column]))
                            tempPossibleValueList.Add((string)row[column]);
                    }
                }
            }

            return tempPossibleValueList;
        }
        #endregion

        public bool checkIfAttributeExists(TreeAttribute Attribute)
        {
            bool exists = false;
            foreach (TreeAttribute att in getAttribiutes())
            {
                if (att.Equals(Attribute))
                    exists = true;
            }
            return exists;
        }

        public List<TreeAttribute> getAttribiutes()
        {
            List<TreeAttribute> listOfAttributes = new List<TreeAttribute>();

            foreach (DataColumn column in this.Columns)
            {
                listOfAttributes.Add(new TreeAttribute(column.ColumnName, getPossibleValuesOfColumn(column.ColumnName), column.Ordinal));
            }

            return listOfAttributes;
        }

    }
}
