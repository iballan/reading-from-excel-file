﻿namespace BUM_FINAL_ID3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.open_button = new System.Windows.Forms.Button();
            this.possibleValues_button = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.createTreeButton = new System.Windows.Forms.Button();
            this.treeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.label3 = new System.Windows.Forms.Label();
            this.showOptimalPath = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // open_button
            // 
            this.open_button.Location = new System.Drawing.Point(12, 14);
            this.open_button.Name = "open_button";
            this.open_button.Size = new System.Drawing.Size(120, 23);
            this.open_button.TabIndex = 0;
            this.open_button.Text = "Open";
            this.open_button.UseVisualStyleBackColor = true;
            this.open_button.Click += new System.EventHandler(this.open_button_Click);
            // 
            // possibleValues_button
            // 
            this.possibleValues_button.Location = new System.Drawing.Point(138, 14);
            this.possibleValues_button.Name = "possibleValues_button";
            this.possibleValues_button.Size = new System.Drawing.Size(120, 23);
            this.possibleValues_button.TabIndex = 1;
            this.possibleValues_button.Text = "Possible Values";
            this.possibleValues_button.UseVisualStyleBackColor = true;
            this.possibleValues_button.Click += new System.EventHandler(this.possibleValues_button_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 43);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(830, 264);
            this.dataGridView1.TabIndex = 2;
            // 
            // resultTextBox
            // 
            this.resultTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.resultTextBox.Location = new System.Drawing.Point(12, 332);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultTextBox.Size = new System.Drawing.Size(280, 158);
            this.resultTextBox.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(395, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // createTreeButton
            // 
            this.createTreeButton.Location = new System.Drawing.Point(265, 14);
            this.createTreeButton.Name = "createTreeButton";
            this.createTreeButton.Size = new System.Drawing.Size(120, 23);
            this.createTreeButton.TabIndex = 5;
            this.createTreeButton.Text = "Create Tree";
            this.createTreeButton.UseVisualStyleBackColor = true;
            this.createTreeButton.Click += new System.EventHandler(this.createTreeButton_Click);
            // 
            // treeTextBox
            // 
            this.treeTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.treeTextBox.Location = new System.Drawing.Point(567, 332);
            this.treeTextBox.Multiline = true;
            this.treeTextBox.Name = "treeTextBox";
            this.treeTextBox.ReadOnly = true;
            this.treeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.treeTextBox.Size = new System.Drawing.Size(275, 158);
            this.treeTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 313);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Log:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(564, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tree String:";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(298, 332);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(263, 158);
            this.treeView1.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(295, 313);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Tree View:";
            // 
            // showOptimalPath
            // 
            this.showOptimalPath.Location = new System.Drawing.Point(720, 12);
            this.showOptimalPath.Name = "showOptimalPath";
            this.showOptimalPath.Size = new System.Drawing.Size(120, 23);
            this.showOptimalPath.TabIndex = 11;
            this.showOptimalPath.Text = "Show Optimal Path";
            this.showOptimalPath.UseVisualStyleBackColor = true;
            this.showOptimalPath.Click += new System.EventHandler(this.showOptimalPath_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 502);
            this.Controls.Add(this.showOptimalPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeTextBox);
            this.Controls.Add(this.createTreeButton);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.possibleValues_button);
            this.Controls.Add(this.open_button);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button open_button;
        private System.Windows.Forms.Button possibleValues_button;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button createTreeButton;
        private System.Windows.Forms.TextBox treeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button showOptimalPath;
    }
}

