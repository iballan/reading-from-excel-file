﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using BUM_FINAL_ID3.Structure;

namespace BUM_FINAL_ID3.Structure
{
    public class Tree  : TreeNode
    {
        //private TreeNode node;

        //public Tree(TreeAttribute rootAttribute, string root) : base(rootAttribute, root) { }
        public Tree(string root) : base(root) { }

        public string BuildString()
        {
            var sb = new StringBuilder();

            BuildString(sb, this, 0);
            
            return sb.ToString();
        }

        private void BuildString(StringBuilder sb, TreeNode node, int depth)
        {
            sb.AppendLine(node.ID.PadLeft(node.ID.Length + depth*4));
            int childCount = node.Count;
            foreach (var child in node)
            {
                BuildString(sb, child, depth + 1);
            }
        }


        public void drawTree(TreeView treeView){
            System.Windows.Forms.TreeNode bigParent;// = new System.Windows.Forms.TreeNode(this.ID);
            int childCount = this.Count;
            if (childCount != 1)
                return;
            else{
                foreach (var child in this)
                {
                    bigParent = new System.Windows.Forms.TreeNode(child.ID);
                    BuildTreeView(child, bigParent);
                    treeView.Nodes.Add(bigParent);
                }
                
            }
        }

        private void BuildTreeView( TreeNode node, System.Windows.Forms.TreeNode parentNode)
        {
           
            //treeView.Nodes.Add(new System.Windows.Forms.TreeNode(node.ID));
            foreach (var child in node)
            {
                System.Windows.Forms.TreeNode smallParent = new System.Windows.Forms.TreeNode(child.ID);
                BuildTreeView( child, smallParent);
                parentNode.Nodes.Add(smallParent);
            }
        }
    }
}
