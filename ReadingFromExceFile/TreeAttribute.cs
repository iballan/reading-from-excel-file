﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUM_FINAL_ID3
{
    public class TreeAttribute
    {
        private string _name;
        private List<string> _possibleValues;
        private int _columnPosition = -1;

        public TreeAttribute(string name, List<string> possibleValues, int columnPosition)
        {

            _columnPosition = columnPosition;
            _name = name;
            if (possibleValues == null)
                _possibleValues = null;
            else
            {
                _possibleValues = possibleValues;
                _possibleValues.Sort();
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }
        public int ColumnPosition
        {
            get
            {
                return _columnPosition;
            }
            set
            {
                _columnPosition = value;
            }
        }
        public List<string> PossibleValues
        {
            get
            {
                return _possibleValues;
            }
        }

        public bool isValidValue(string value)
        {
            return (_possibleValues.BinarySearch(value) >= 0);
        }

        public int indexValue(string value)
        {
            if (_possibleValues != null)
            {
                return _possibleValues.BinarySearch(value);
            }
            else
            {
                return -1;
            }
        }

        public override string ToString()
        {
            return _name;
        }

        public override bool Equals(System.Object obj)
        { 
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to TreeAttribute return false.
            TreeAttribute p = obj as TreeAttribute;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (_name == p.Name) && (_columnPosition == p.ColumnPosition) && (_possibleValues.Count == p.PossibleValues.Count);
        }
    }
}
