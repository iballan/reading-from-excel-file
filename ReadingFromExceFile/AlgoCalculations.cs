﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace BUM_FINAL_ID3
{
    class AlgoCalculations
    {
        /// <summary>
        /// Will calculate the entropy from array of tuplse<string, int>
        /// </summary>
        /// <param name="arrayOfTuples">Array of tuples<string, int>.</param>
        /// <returns>Entropy as double.</returns>
        public static double calculateEntropyForTuplesArray(Tuple<string, int>[] arrayOfTuples)
        {
            int length = arrayOfTuples.Length;
            int[] occuerences = getOccuerrencesArrayOfTuplesArray(arrayOfTuples);

            return calculateEntropy(occuerences);
        }


        public static Tuple<string, int>[] getOccurrenceOfAttrinbuteAsTupleArray(TreeDataSource samples, TreeAttribute Attribute)
        {
            if (Attribute.PossibleValues.Count < 1)
                return null;

            // occurrence will be calculated for each possible value
            int[] occuerences = new int[Attribute.PossibleValues.Count];

            var res = (from x in samples.AsEnumerable()
                       group x by (string)x[Attribute.Name] into y
                       select Tuple.Create(y.Key, y.Count())).ToArray();

            return res;
        }

        public static int[] getOccurrenceOfAttrinbute(TreeDataSource sample,TreeAttribute Attribute)
        {

            if (Attribute.PossibleValues.Count < 1)
                return null;

            // occurrence will be calculated for each possible value
            int[] occuerences = new int[Attribute.PossibleValues.Count];

            var res = (from x in sample.AsEnumerable()
                       group x by (string)x[Attribute.Name] into y
                       select Tuple.Create(y.Key, y.Count())).ToArray();
            int index = 0;
            foreach (var item in res)
            {
                occuerences[index] = item.Item2;
                index++;
            }
            return occuerences;
        }

        // check if all column of attribute are same
        public static bool isAllValuesSame(TreeDataSource sample, TreeAttribute attribute)
        {
            if (attribute == null)
                return true;

            var attributeColumn = sample.AsEnumerable()
                .Select(s => s.Field<string>(attribute.Name))
                .Distinct()
                .ToList();


            var first = attributeColumn.First();

            return attributeColumn.All(x => x == first);
        }

        public static int[] getOccuerrencesArrayOfTuplesArray(Tuple<string, int>[] arrayOfTuples)
        {
            if (arrayOfTuples == null)
                return new int[] { 0 };

            if (arrayOfTuples.Length == 0)
                return new int[] { 0 };
            int length = arrayOfTuples.Length;
            int[] occuerences = new int[length];
            for (int i = 0; i < length; i++)
                occuerences[i] = arrayOfTuples[i].Item2;
            return occuerences;
        }

        public static double TotalOfTupleArrays(Tuple<string, int>[] arrayOfTuples)
        {
            int[] occuerences = getOccuerrencesArrayOfTuplesArray(arrayOfTuples);
            return TotalOfOccuerencesArray(occuerences);
        }

        public static double TotalOfOccuerencesArray(int[] occurrences)
        {
            double totalOccurrences = 0;
            int counter = 0;

            // calculate total Occurences
            for (int i = 0; i < occurrences.Length; i++)
            {
                if (occurrences[i] == 0)
                    counter++;
                totalOccurrences += (double)occurrences[i];
            }

            // wrong input
            if(occurrences.Length > 1)
                if (counter == occurrences.Length - 1)
                    return 0;
            // wrong input
            if (totalOccurrences == 0)
                return 0;

            return totalOccurrences;

        }

        /// <summary>
        /// Will calculate the entropy from array of occurrences<string, int>
        /// </summary>
        /// <param name="occurrences">Array of int contains the occuerences<string, int>.</param>
        /// <returns>Entropy as double.</returns>
        public static double calculateEntropy(int[] occurrences)
        {
            double totalOccurrences = TotalOfOccuerencesArray(occurrences);

            if (totalOccurrences == 0)
                return 0;

            double entropy = 0;
            for (int i = 0; i < occurrences.Length; i++)
            {
                // entropy equation
                if (occurrences[i] != 0)
                    entropy += calculateEntropyForOneVariable(occurrences[i], totalOccurrences);
            }

            return entropy;
        }

        public static double calculateEntropy(TreeDataSource samples, TreeAttribute attribute)
        {
            int[] occuerrences = AlgoCalculations.getOccurrenceOfAttrinbute(samples ,attribute);
            return calculateEntropy(occuerrences);
        }

        /// <summary>
        /// Will calculate the entropy for one value<string, int>
        /// </summary>
        /// <param name="occurrence">int represents the occuerence of an item<string, int>.</param>
        /// <param name="total">total of all occuerences<string, int>.</param>
        /// <returns>Entropy as double.</returns>
        public static double calculateEntropyForOneVariable(double occurrence, double total)
        {
            double p = occurrence / total;
            //entropy += calculateEntropyForOneVariable(occurrences[i], totalOccurrences);
            //double entropy = -p * Math.Log( p , 2);
            double entropy = (-1) * (p)
                * (Math.Log10(p) / Math.Log10(2));
            return entropy;
        }

        /// <summary>
        /// check whether all values are same<string, int>
        /// </summary>
        /// <param name="arrayOfTuples">array of tuples that have numbers of occuerences for all values<string, int>.</param>
        /// <returns>null if not all are same. otherwise, it will return the tuple itself which is all values r same.</returns>
        public static Tuple<string,int> isAllValuesAreSame(Tuple<string, int>[] arrayOfTuples)
        {
            if (arrayOfTuples.Length == 0)
                return null;

            if (arrayOfTuples == null)
                return null;

            Tuple<string, int> resultTuple = null;

            int totalOfOccuerences = (int)TotalOfTupleArrays(arrayOfTuples);

            foreach (Tuple<string, int> tuple in arrayOfTuples)
            {
                if (tuple.Item2 == totalOfOccuerences)
                    resultTuple = tuple;
            }

            return resultTuple;
        }

        public static double calculateGainInformation(TreeDataSource samples,
            TreeAttribute attribute, double classAttributeEntropy, TreeAttribute targetAttribute)
        {
            // IG(A,S) = H(S) - \sum_{t \in T} p(t)H(t) 

            // we already have H(S) from parameters (classAttributeEntropy)
            Dictionary<string, double> possibleValueEntropy = new Dictionary<string, double>();
            Dictionary<string, int> possibleValueOccurrence = new Dictionary<string, int>();

            int numberOfClassifications = targetAttribute.PossibleValues.Count;
            List<string> possibleValues = attribute.PossibleValues;

            // this list will get back only the weather sunny and play
            foreach (string possibleValue in possibleValues)
            {
                int occuerenceOfThisPossibleValue = 0;
                double entropyForThisValue = calculateEntropyForPossibleValue
                    (samples, attribute, possibleValue, numberOfClassifications, targetAttribute ,ref occuerenceOfThisPossibleValue);
                
                possibleValueEntropy.Add(possibleValue, entropyForThisValue);
                possibleValueOccurrence.Add(possibleValue, occuerenceOfThisPossibleValue);
            }

            int totalOccurence = 0;
            foreach (int value in possibleValueOccurrence.Values)
            {
                totalOccurence = totalOccurence + value;
            }
            // sum_{t \in T} p(t)H(t) 
            double sumOfEntropies = 0;
            foreach (var keyValue in possibleValueEntropy)
            {
                sumOfEntropies += keyValue.Value * possibleValueOccurrence[keyValue.Key] / totalOccurence;
            }

            // now to the result !
            // IG(A,S) = H(S) - \sum_{t \in T} p(t)H(t) 
            return classAttributeEntropy - sumOfEntropies;
        }

        public static double calculateEntropyForPossibleValue(TreeDataSource samples, TreeAttribute attribute,
            string possibleValue, int numberOfClassifications, TreeAttribute targetAttribute, ref int occOfThisValue)
        {
            var AttributeWithTarget = samples.Rows.Cast<DataRow>()
                    .Where(x => x.Field<string>(attribute.Name) == possibleValue)
                    .Select(x => new
                    {
                        Attribute = x.Field<string>(attribute.Name),
                        Classification = x.Field<string>(targetAttribute.Name)
                    })
                    .ToList();

            // this value passed by reference
            occOfThisValue = AttributeWithTarget.Count;
            //string outPutStringForTesting = string.Format("Attribute is: {0},Value {1} = {2}.....Classes are: ",
            //    attribute.Name, AttributeWithTarget.First().Attribute.ToString(),
            //    AttributeWithTarget.Count);
            //int[] occuerrences = 
            var groups = AttributeWithTarget.GroupBy(x => x.Classification);

            int[] occuerrences = new int[numberOfClassifications];
            int counter = 0;
            foreach (var groupped in groups)
            {
                int groupCount = groupped.Count();
                occuerrences[counter] = groupCount;
                //outPutStringForTesting += string.Format("{0} = {1}, ", groupped.Key, groupCount);
                counter++;
            }
            double entropyForThisValue = calculateEntropy(occuerrences);

            return entropyForThisValue;
        }

    }
}
